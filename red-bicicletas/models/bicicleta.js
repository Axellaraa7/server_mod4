var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var bicicletaSchema = new Schema({
	code: {
		type: Number,
		required: true,
	},
	color: {
		type: String,
		required: true,
	},
	modelo: {
		type: String,
		required: true,
	},
	ubicacion: {
		type: [Number],
		index: { type: "2dsphere", sparse: true }, // se crea un indice de tipo geografico (2dsphere)
	},
});

bicicletaSchema.methods.toString = function(){
	return 'code: '+ this.code + ' | color: ' + this.color;
};

bicicletaSchema.statics.allBicis = function(cb){
	return this.find({}, cb);
};

bicicletaSchema.statics.createInstance = function(code,color,modelo,ubicacion){
	return new this({
		code: code,
		color: color,
		modelo: modelo,
		ubicacion: ubicacion
	});
};

bicicletaSchema.statics.add = function(bici,cb){
	this.create(bici,cb);
};

bicicletaSchema.statics.findByCode = function(code,cb){
	return this.findOne({code: code}, cb);
}

bicicletaSchema.statics.removeByCode = function(code,cb){
	return this.deleteOne({code: code}, cb);
}

bicicletaSchema.statics.updateBici = function(bici,cb){
	return this.updateOne({_id: bici._id},{
		code: bici.code,
		modelo: bici.modelo,
		color: bici.color,
		ubicacion: bici.ubicacion
	},cb);
}

module.exports = mongoose.model('Bicicleta', bicicletaSchema);

/*

var Bicicleta = function (id,color,modelo,ubicacion){
	this.id = id;
	this.color = color;
	this.modelo = modelo;
	this.ubicacion = ubicacion;
}


Bicicleta.allBicis = [];

Bicicleta.add = function(aBici){
	Bicicleta.allBicis.push(aBici);
}

Bicicleta.findById = function(id){
	var bici = Bicicleta.allBicis.find(x => x.id == id);
	if(bici)
		return bici;
	else
		throw new Error("No existe ese elemento");
}

Bicicleta.remove = function(id){
	for(var i=0; i<Bicicleta.allBicis.length; i++){
		if(Bicicleta.allBicis[i].id === parseInt(id)){
			Bicicleta.allBicis.splice(i,1);
			break;
		}
	}
}

/*
var bici1 = new Bicicleta (1, 'rojo', 'urbana', [19.43249555151372, -99.13111814447217]);
var bici2 = new Bicicleta(2, 'azul', 'urbana'   , [19.43335049138733, -99.12979313331095]);
     
Bicicleta.add(bici1);
Bicicleta.add(bici2);


module.exports = Bicicleta;*/