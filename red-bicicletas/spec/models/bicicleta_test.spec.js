var mongoose = require ('mongoose');
var Bicicleta = require('../../models/bicicleta');

describe('Testing Bicicletas', function(){
	beforeEach(function(done){
		var mongoDB = 'mongodb://localhost/testdb';
		mongoose.connect(mongoDB, { useNewUrlParser: true });

		const db = mongoose.connection;
		db.on('error', console.error.bind(console, 'connection error'));
		db.once('open',function(){
			console.log('Conectados...');
			done();
		});
	});

	afterEach(function(done){
		Bicicleta.deleteMany({}, function(err,success){
			if(err) console.log(err);
			done();
		});
	});

	describe('Biciclet.createInstance',() => {
		it('Crea una instancia de bicicleta', () => {
			var bici1 =Bicicleta.createInstance(1, 'rojo', 'urbana', [19.43249555151372, -99.13111814447217]);

			expect(bici1.code).toBe(1);
			expect(bici1.color).toBe('rojo');
			expect(bici1.modelo).toBe('urbana');
			expect(bici1.ubicacion[0]).toEqual(19.43249555151372);
			expect(bici1.ubicacion[1]).toEqual(-99.13111814447217);
		});
	});

	describe('Bicicleta.allBicis', () => {
		it('Comienza vacía', (done) =>{
			Bicicleta.allBicis(function(err,bicis){
				expect(bicis.length).toBe(0);
				done();
			});
		});
	});

	describe('Bicicleta.add',() =>{
		it("Agregar solo una bici", (done) =>{
			var bici = new Bicicleta({code:1,color:"verde", modelo:"urbana"});
			Bicicleta.add(bici, function(err,newBici){
				if(err) console.log(err);
				Bicicleta.allBicis(function(err,bicis){
					expect(bicis.length).toEqual(1);
					expect(bicis[0].code).toEqual(bici.code);
					done();
				})
			});
		});
	});

	describe('Bicicleta.findByCode', () =>{
		it('Debe devolver la bici con el codigo', (done) => {
			Bicicleta.allBicis(function(err,bicis){
				expect(bicis.length).toBe(0);

				var bici = new Bicicleta({code:1,color:"verde", modelo:"urbana"});
				Bicicleta.add(bici, function(err,newBici){
					if(err) console.log(err);
					Bicicleta.findByCode(1, function(err,bicis){
						expect(bicis.code).toBe(bici.code);
						expect(bicis.color).toBe(bici.color);
						expect(bicis.modelo).toBe(bici.modelo);
						done();
					});
				});
			});
		});
	});

	describe('Bicicleta.removeByCode', () =>{
		it('Debe eliminar la bici con el codigo', (done) => {
			var bici = new Bicicleta({code:1,color:"verde", modelo:"urbana"});
			Bicicleta.add(bici,function(err,newBici){
				if(err) console.log(err);
				Bicicleta.allBicis(function(err,bicis){
				expect(bicis.length).toBe(1);
				});
				Bicicleta.removeByCode(1, function(err,bicis){
					Bicicleta.allBicis(function(err,bicis){
						expect(bicis.length).toBe(0);
						done();
					});
				});
			});
		});
	});		
});

/*beforeEach(() => { Bicicleta.allBicis = []; 
console.log('testeando...')});

describe('Bicicleta.allBicis',() => {
	it('Comienza vacía', () => {
		expect(Bicicleta.allBicis.length).toBe(0);
	});
});

describe('Bicicleta.add', () => {
	it("Agregamos una", () => {
		expect(Bicicleta.allBicis.length).toBe(0);

		var bici1 = new Bicicleta (1, 'rojo', 'urbana', [19.43249555151372, -99.13111814447217]);
		Bicicleta.add(bici1);

		expect(Bicicleta.allBicis.length).toBe(1);
		expect(Bicicleta.allBicis[0]).toBe(bici1);
	});
});

describe('Bicicleta.findById',() => {
	it("Debe devolver la bici con el id correspondiente", () => {
		expect(Bicicleta.allBicis.length).toBe(0);

		var bici1 = new Bicicleta (1, 'rojo', 'urbana', [19.43249555151372, -99.13111814447217]);

		Bicicleta.add(bici1);

		var targetBici = Bicicleta.findById(1);
		expect(targetBici.id).toBe(1);
		expect(targetBici.color).toBe(bici1.color);
		expect(targetBici.modelo).toBe(bici1.modelo);
	});
});

describe('Bicicleta.remove',() => {
	it("Debe eliminar un elemento", () => {
		expect(Bicicleta.allBicis.length).toBe(0);

		var bici2 = new Bicicleta (2, 'rojo', 'urbana', [19.43249555151372, -99.13111814447217]);
		Bicicleta.add(bici2);

		expect(Bicicleta.allBicis.length).toBe(1);

		Bicicleta.remove(2);

		expect(Bicicleta.allBicis.length).toBe(0);
	});
});*/
		
				