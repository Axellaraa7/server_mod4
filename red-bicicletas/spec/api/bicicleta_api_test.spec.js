var Bicicleta = require('../../models/bicicleta');
var Request = require('request');
var Server = require('../../bin/www');
var mongoose = require('mongoose');

var base_url= "http://localhost:3000/api/bicicletas";

/*beforeEach(() => {  
	Bicicleta.allBicis = []; 
	console.log('testeando...')
});*/

describe('Bicicleta API', () =>{
	beforeEach(function(done) {
		var mongoDB = 'mongodb://localhost/testdb';
		mongoose.connect(mongoDB, { useNewUrlParser: true });

		const db = mongoose.connection;
		db.on('error', console.error.bind(console, 'connection error'));
		db.once('open', function(){
			console.log('Conectados');
			done();
		});
	});

	afterEach(function(done){
		Bicicleta.deleteMany({}, function(err,success){
			if(err) console.log(err);
			done();
		});
	});


	describe("GET", () =>{
		it("Status 200", (done) =>{
			request.get(base_url, function(error,response,body){
				var result = JSON.pase(body);
				expect(response.statusCode).toBe(200);
				expect(result.bicicletas.length).toBe(0);
				done();
			});
		});
	});

	describe("POST /CREATE", () => {
		it("STATUS 200", (done) => {
			var headers = {'content-type': "application/json"};
			var aBici = '{"code": 10, "color": "rojo", "modelo":"urbana", "lat":-34, "long":-57 }';
			Request.post({
				headers: headers,
				url: 'http://localhost:3000/api/bicicletas/create',
				body: aBici
			}, function(error,response,body){
				expect(response.statusCode).toBe(200);
				var bici = JSON.parse(body).bicicletas;
				console.log(bici);
				//expect(Bicicleta.findById(10).color).toBe("rojo");
				expect(bici.color).toBe("rojo");
				done();
			});
		});
	});

	describe("DELETE /DELETE", () =>{
		it("STATUS 204", (done) => {
			var headers = {'content-type': "application/json"};
			var bici1 = new Bicicleta (1, 'rojo', 'urbana', [19.43249555151372, -99.13111814447217]);
			Bicicleta.add(bici1);
			var deleteBici = '{"id": 1}';
			Request.delete({
				headers:headers,
				url: 'http://localhost:3000/api/bicicletas/delete',
				body: deleteBici
			}, function(error,response,body){
				expect(response.statusCode).toBe(204);
				expect(Bicicleta.allBicis.length).toBe(0);
				done();
			});
		});
	});

	describe("POST /UPDATE", () => {
		it("STATUS 200", (done) => {
			var headers = {'content-type': "application/json"};
			var bici1 = new Bicicleta (1, 'rojo', 'urbana', [19.43249555151372, -99.13111814447217]);
			Bicicleta.add(bici1);
			var updateBici = '{ "id": 1, "color":"morada", "modelo"="montaña", "lat"=19.43249555151372, "long"=-99.13111814447217 }';
			Request.post({
				headers: headers,
				url: 'http://localhost:3000/api/bicicletas/update',
				body: updateBici
			}, function(error,response,body){
				expect(response.statusCode).toBe(200);
				done();
			});
		});
	});
});

