var map= L.map('main_map').setView([19.42610885,-99.1866289270503], 10);

L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png',{
	attribution: '&copy; <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
}).addTo(map);

 //para agregar marcadores en el mapa

$.ajax({
	dataType: "json",
	url: "api/bicicletas",
	success: function(result){
		console.log(result);
		result.bicicletas.forEeach(function(bici){
			L.marker(bici.ubicacion, {title: bici.id}).addTo(map)
		});
	}
})