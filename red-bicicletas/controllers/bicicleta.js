var Bicicleta = require('../models/bicicleta');

exports.bicicleta_list = function(req,res){
	Bicicleta.allBicis((err, bicicletas) => {
		res.render("bicicletas/index", { bicis: bicicletas });
	});
}

exports.bicicleta_create_get = function(req, res){
	res.render('bicicletas/create');
}

exports.bicicleta_create_post = function(req, res){
	ubicacion = [req.body.lat, req.body.long];
	var bici = new Bicicleta({code: req.body.id, color: req.body.color, modelo:req.body.modelo, ubicacion:ubicacion});
	Bicicleta.add(bici);
	console.log("Bicicleta creada");

	res.redirect('/bicicletas');
}

exports.bicicleta_delete_post = function(req,res){
	Bicicleta.removeByCode(req.params.id,function(err,bici){
		res.redirect('/bicicletas');
	});
}

exports.bicicleta_update_get = function(req,res){
	console.log(req.params.id);
	Bicicleta.findByCode(req.params.id, (err, bici) => {
		res.render("bicicletas/update", { bici });
	});
}

exports.bicicleta_update_post = function(req,res){
	Bicicleta.findByCode(req.params.id, (err, bici) => {
		console.log('Estamos en: '+bici.code);
		bici.code = req.body.id;
		bici.color = req.body.color;
		bici.modelo = req.body.modelo;
		bici.ubicacion = [req.body.lat, req.body.long];
		console.log(bici.code + ' '+ bici.color);
		Bicicleta.updateBici(bici, (err, bici1) => {
			if (err) console.log(err);

			console.log(bici)
			console.log(bici1)
			res.redirect("/bicicletas");
		});
	});


}